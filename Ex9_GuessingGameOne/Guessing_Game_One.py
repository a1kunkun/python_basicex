import random
'''
@author : Nguyen Phuoc Toan
@since : 20/02/2019
@version : 1.0

@exercise 9 : Guessing Game One
Generate a random number between 1 and 9 (including 1 and 9).
Ask the user to guess the number,
then tell them whether they guessed too low, too high, or exactly right.
(Hint: remember to use the user input lessons from the very first exercise)

Extras:

- Keep the game going until the user types “exit”
- Keep track of how many guesses the user has taken,
and when the game ends, print this out.
'''
# Tao ra 1 so ngau nhien
a = random.randint(1, 9)

# In ra a de kiem tra function
print(a)

d = 1
b = 0

# Tao vong lap so sanh ket qua
while a != b:
    b = input("Nhap vao so ban doan : ")
    # Kiem tra player nhap vao exit thi thoat chuong trinh
    if b == "exit":
        print("ban da thoat")
        break
    # Doi kieu str sang int
    b = int(b)
    # So sanh so input voi so random
    if b > a:
        print("So ban vua nhap vao lon hon")
        # Tinh so lan doan
        d += 1
    elif b < a:
        print("So ban vua nhap vao be hon")
        d += 1
    else:
        print("Ban da doan dung, " + str(a) + " la ket qua")
        print("Ban da doan : " + str(d) + " lan ")
