'''
@author : Nguyen Phuoc Toan
@since : 22/02/2019
@version : 1.0

@exercise 15 : Reverse Word Order
Write a program (using functions!) that asks the user for
a long string containing multiple words. Print back to the user the same string
except with the words in backwards order. For example, say I type the string:

  My name is Michele
Then I would see the string:

  Michele is name My
shown back to me.
'''


# Nhap vao day string
def createString():
    str1 = input("Nhap vao dong chu ban muon :")
    list1 = str1.split(' ')
    return list1


# Function dao nguoc lai chuoi string
def backStr(list1):
    # Dao nguoc list
    list1.reverse()
    # Chuyen tu list sang str
    list2 = ' '.join(list1)
    return print(list2)


# Xuat ra ket qua de kiem tra
backStr(createString())
