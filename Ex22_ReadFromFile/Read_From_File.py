'''
@author : Nguyen Phuoc Toan
@since : 11/03/2019
@version : 1.0

@exercise 22 : Read From File
Given a .txt file that has a list of a bunch of names,
count how many of each name there are in the file,
and print out the results to the screen.
I have a .txt file for you, if you want to use it!

Extra:

Instead of using the .txt file from above (or instead of,
if you want the challenge), take this .txt file, and count
how many of each “category” of each image there are.
This text file is actually a list of files corresponding
to the SUN database scene recognition database, and lists
the file directory hierarchy for the images. Once you take a look at
the first line or two of the file, it will be clear which part represents
the scene category. To do this, you’re going to have to remember a bit
about string parsing in Python 3. I talked a little bit about it in this post.
'''
# Tao 1 dic luu tru gom {ten: so luong}
counter_dict = {}

# Mo file can doc
with open('/hoctap/Python_Basic_Exercise/python_basicex/\
Ex22_ReadFromFile/nameslist.txt') as f:
    # Doc dong dau tien trong file
    line = f.readline()
    # Kiem tra line
    while line:
        # Bo ki tu \n
        line = line.strip()
        # Kiem tra co bao nhieu ten va so luong bao nhieu
        if line in counter_dict:
            counter_dict[line] += 1
        else:
            counter_dict[line] = 1
        line = f.readline()

# Xuat ra ket qua kiem tra
print(counter_dict)
