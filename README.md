# Python Basic Exercise

This is the solution of 52 Python Basic Exercise.

## Sources

- [Practice Python](https://www.practicepython.org/).

## Usage
```python

Git clone from : https://gitlab.com/a1kunkun.
Run Visual Studio Code.
Run any Exercise what you want.
```

## Built With

* Visual Studio Code.
* Python 3.7.4.

## Authors

* **Nguyen Phuoc Toan** - *Initial work* - [Gitlab](https://gitlab.com/a1kunkun).

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
