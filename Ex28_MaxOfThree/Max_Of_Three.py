'''
@author : Nguyen Phuoc Toan
@since : 11/03/2019
@version : 1.0

@exercise 28 : Max Of Three
Implement a function that takes as input three variables,
and returns the largest of the three.
Do this without using the Python max() function!

The goal of this exercise is to think about some internals
that Python normally takes care of for us.
All you need is some variables and if statements!
'''
# Nhap vao 3 so
num1 = input("Nhap vao so thu 1: ")
num2 = input("Nhap vao so thu 2: ")
num3 = input("Nhap vao so thu 3: ")

# Gia su so lon nhat la num1
max = num1

# Kiem tra so lon nhat
if num1 < num2:
    max = num2
elif num2 < num3:
    max = num3

# In ra ket qua
print("So lon nhat la: " + max)
