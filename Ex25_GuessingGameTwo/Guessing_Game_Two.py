'''
@author : Nguyen Phuoc Toan
@since : 11/03/2019
@version : 1.0

@exercise 25 : Guessing Game Two
In a previous exercise,
we’ve written a program that “knows” a number and asks a user to guess it.

This time, we’re going to do exactly the opposite.
You, the user, will have in your head a number between 0 and 100.
The program will guess a number, and you, the user,
will say whether it is too high, too low, or your number.

At the end of this exchange,
your program should print out how many guesses it took to get your number.

As the writer of this program,
you will have to choose how your program will strategically guess.
A naive strategy can be to simply start the guessing at 1,
and keep going (2, 3, 4, etc.) until you hit the number.
But that’s not an optimal guessing strategy.
An alternate strategy might be to guess 50 (right in the middle of the range),
and then increase / decrease by 1 as needed. After you’ve written the program,
try to find the optimal strategy!
(We’ll talk about what is the optimal one next week with the solution.)
'''


def guessing_num():
    system_num = 50
    running = True
    while running:
        print("Computer doan la : " + str(system_num))
        a = input("Nhap ket qua cua ban (too high, too low,\
            gia tri cua my_number): ")
        # Khi nhap vao too high thi so computer doan se - 11
        if a == "too high":
            system_num -= 1
        # Khi nhap vao too low thi so computer doan se + 1
        elif a == "too low":
            system_num += 1
        # Neu nhap dung thi dung lai ket qua
        elif system_num == my_number:
            print('computer da doan dung')
            running = False


if __name__ == "__main__":
    # Nhap so muon computer doan dung
    my_number = 52
    guessing_num()
