'''
@author : Nguyen Phuoc Toan
@since : 11/03/2019
@version : 1.0

@exercise 21 : Write To A File
Take the code from the How To Decode A Website exercise
(if you didn’t do it or just want to play with some different code,
use the code from the solution), and instead of printing
the results to a screen, write the results to a txt file. In your code,
just make up a name for the file you are saving to.

Extras:

Ask the user to specify the name of the output file that will be saved.
'''
import requests
from bs4 import BeautifulSoup

# Decode a webpage ex17
# Truy xuat trang web
result = requests.get("https://www.nytimes.com/")

# Xuat ra ket qua la cac dong text
c = result.text
soup = BeautifulSoup(c, "html.parser")

# Truy xuat nhung tieu de la the h2
samples = soup.find_all('h2')
data = []

# Luu du lieu vua scraper vao 1 list
for sub_heading in samples:
    print(sub_heading.text)
    data.append(sub_heading.text)

# Nguoi dung nhap ten file txt
txtname = input("Nhap ten txt ma ban muon ")

# Viet noi dung vao file txt
filename = f'/hoctap/Python_Basic_Exercise/python_basicex/Ex21_WriteToAFile/\
{txtname}.txt'
with open(filename, "w", encoding="utf-8") as open_file:
        for i in data:
            open_file.write(i)
