'''
@author : Nguyen Phuoc Toan
@since : 19/02/2019
@version : 1.0

@exercise 1 : Character Input
Create a program that asks the user to enter their name and their age.
Print out a message addressed to them that tells them the year
 that they will turn 100 years old.
'''

# Nhap ten
name = input("Vui long nhap ten: ")
print("Ten cua ban la : " + name)

# Nhap tuoi
age = int(input("Vui long nhap so tuoi: "))

# Tinh nam sinh
bnyear = 2019 - age

# Tinh so nam dat 100 tuoi
year100 = bnyear + 100
print("So nam ban dat 100 tuoi la: " + str(year100))
