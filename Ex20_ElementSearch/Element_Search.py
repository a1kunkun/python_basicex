'''
@author : Nguyen Phuoc Toan
@since : 11/03/2019
@version : 1.0

@exercise 20 : Element Search
Write a function that takes an ordered list of numbers
(a list where the elements are in order from smallest to largest)
and another number. The function decides whether or not
the given number is inside the list and returns
(then prints) an appropriate boolean
'''


def findnumber(list_, num):
    for i in list_:
        if i == num:
            return True
    return False


if __name__ == "__main__":
    list1 = [5, 6, 7, 10, 8]
    input_num = int(input("Nhap vao so ban can tim: "))
    if findnumber(list1, input_num) is True:
        print("So ban can tim " + str(input_num) + " co trong day")
    else:
        print("So ban can tim " + str(input_num) + " khong co trong day")
