'''
@author : Nguyen Phuoc Toan
@since : 21/02/2019
@version : 1.0

@exercise 11 : Check Primality Functions
Ask the user for a number and determine whether the number is prime or not.
(For those who have forgotten, a prime number is a number that has no divisors)
You can (and should!) use your answer to Exercise 4 to help you.
Take this opportunity to practice using functions, described below.
'''
# Input mot number
num = int(input("Vui long nhap vao mot so: "))

# Tao ra list bat dau tu 2 den so num - 1
check_num = range(2, num - 1)

# Kiem tra so num co phai la so nguyen to hay khong
if num == 1:
    print("So ban vua nhap : " + str(num) + ", khong phai la so nguyen to")
elif num == 2:
    print("So ban vua nhap : " + str(num) + ", la so nguyen to")
else:
    for i in check_num:
        if num % i == 0:
            print("So ban vua nhap : " + str(num) +
                  ", khong phai la so nguyen to")
        else:
            print("So ban vua nhap : " + str(num) + ", la so nguyen to")
        break
