'''
@author : Nguyen Phuoc Toan
@since : 19/02/2019
@version : 1.0

@exercise 3 : List Less Than Ten
Take a list, say for example this one:

  a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
and write a program that prints out all the elements of the list
that are less than 5.

Extras:

- Instead of printing the elements one by one,
make a new list that has all the elements less than 5
from this list in it and print out this new list.
Write this in one line of Python.
- Ask the user for a number and return a list
that contains only elements from the original list
a that are smaller than that number given by the user.
'''
# Tao list
a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
b = []
c = []

# Kiem tra gia tri < 5
for i in range(len(a)):
    if a[i] < 5:
        print(a[i])
        # BT phan Extras
        # Dua cac phan tu < 5 vao mot list b
        b.append(a[i])

# Nhap vao 1 so
num = int(input("Nhap vao mot so : "))

# So sanh so nhap vao < 5 va dua vao list c
for i in range(len(a)):
    if a[i] < num:
        c.append(a[i])

# Xuat ra list b va c de kiem tra
print(b)
print(c)
