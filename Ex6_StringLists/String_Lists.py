'''
@author : Nguyen Phuoc Toan
@since : 20/02/2019
@version : 1.0

@exercise 6 : String Lists
Ask the user for a string and print out
whether this string is a palindrome or not.
(A palindrome is a string that reads the same forwards and backwards.)
'''
# Nhap vao list
a = [x for x in input("Nhap vao 1 chuoi : ").split()]

# Kiem tra xem co phai chuoi palindrome hay khong
if a == a.reverse():
    print("Day la chuoi palindrome")
else:
    print("Day khong phai la chuoi palindrome")
