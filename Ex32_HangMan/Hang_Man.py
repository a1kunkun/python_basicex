'''
@author : Nguyen Phuoc Toan
@since : 11/03/2019
@version : 1.0

@exercise 32 : Hang Man
This exercise is Part 3 of 3 of the Hangman exercise series.
The other exercises are: Part 1 and Part 2.

You can start your Python journey anywhere, but to finish this exercise
you will have to have finished Parts 1 and 2 or use the solutions
(Part 1 and Part 2).

In this exercise, we will finish building Hangman.
In the game of Hangman, the player only has 6 incorrect guesses
(head, body, 2 legs, and 2 arms) before they lose the game.

In Part 1, we loaded a random word list and picked a word from it.
In Part 2, we wrote the logic for guessing the letter and displaying
that information to the user. In this exercise,
we have to put it all together and add logic for handling guesses.

Copy your code from Parts 1 and 2 into a new file as a starting point.
Now add the following features:

Only let the user guess 6 times,
and tell the user how many guesses they have left.
Keep track of the letters the user guessed.
If the user guesses a letter they already guessed,
don’t penalize them - let them guess again.
Optional additions:

When the player wins or loses,
let them start a new game.
Rather than telling the user "You have 4 incorrect guesses left",
display some picture art for the Hangman. This is challenging -
do the other parts of the exercise first!
Your solution will be a lot cleaner if you make use of functions to help you!
'''

word = "TOAN"

# Tao ra chuoi ki tu "-" kich thuoc bang voi kich thuoc cua word
hided = "-" * len(word)

# Chuyen word va hided thanh list
word = list(word)
hided = list(hided)
running = True
lstGuessed = []
letter = input("guess letter: ")
count = 0
while running:
    # Kiem tra ki tu nhap vao da duoc nhap tu truoc chua
    if letter in lstGuessed:
        letter = ''
        print("Already guessed!!")
        count += 1
        print("Ban con " + str(6 - count) + " lan doan")
    # Kiem tra letter co trong word thi them letter vao hided
    elif letter in word:
        index = word.index(letter)
        hided[index] = letter
        word[index] = '-'
        # count += 1
        # print("Ban con " + str(6 - count) + " lan doan")
    else:
        print(' '.join(hided))
        if letter is not '':
            lstGuessed.append(letter)
            count += 1
            print("Ban con " + str(6 - count) + " lan doan")
        letter = input("guess letter: ")

    if '-' not in hided and count <= 6:
        print("You won!!")
        break
    elif count >= 6:
        print("Ban vuot qua 6 lan doan")
        break
