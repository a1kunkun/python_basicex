'''
@author : Nguyen Phuoc Toan
@since : 11/03/2019
@version : 1.0

@exercise 30 : Pick Word
This exercise is Part 1 of 3 of the Hangman exercise series.
The other exercises are: Part 2 and Part 3.

In this exercise, the task is to write a function
that picks a random word from a list of words from the SOWPODS dictionary.
Download this file and save it in the same directory as your Python code.
This file is Peter Norvig’s compilation of the dictionary of words
used in professional Scrabble tournaments.
Each line in the file contains a single word.

Hint: use the Python random library for picking a random word.
'''
import random

list_word = []
with open('/hoctap/Python_Basic_Exercise/python_basicex/\
Ex30_PickWord/sowpods.txt') as f:
    line = f.readline()
    while line:
        # Bo ki tu \n
        line = line.strip()
        # Dua noi dung doc duoc vao 1 list
        list_word.append(line)
        line = f.readline()

# Chon ngau nhien 1 ten trong list
pick_word = random.choice(list_word)
print(pick_word)
