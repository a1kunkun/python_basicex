import random
import string
'''
@author : Nguyen Phuoc Toan
@since : 22/02/2019
@version : 1.0

@exercise 16 : Password Generator
Write a password generator in Python.
Be creative with how you generate passwords -
strong passwords have a mix of lowercase letters, uppercase letters,
numbers, and symbols. The passwords should be random,
generating a new password every time the user asks for a new password.
Include your run-time code in a main method.

Extra:

- Ask the user how strong they want their password to be.
For weak passwords, pick a word or two from a list.
'''


# Chon muc do password
def passLevel():
    level = input("Vui long chon muc do ban muon: thap, trung binh, cao? ")
    # Chi duoc nhap : cao, trung binh, thap
    while (level != "cao") and (level != "trung binh") and (level != "thap"):
        level = input("Chi duoc chon : thap, trung binh, cao. ")
    return level


# Chon chieu dai password
def passLength():
    length = int(input("Do dai password ban muon la : "))
    return length


# Tao ra pass
def passWord(level, length):
    if level == "cao":
        # Tao ra 1 day string la tat ca bang chu cai viet thuong,
        # chu cai viet hoa, ki hieu, kieu so
        full = string.ascii_lowercase + string.ascii_uppercase
        full = string.punctuation + string.digits
        # Tao ra 1 day string la tat ca bang chu cai viet thuong,
        # chu cai viet hoa,kieu so
    elif level == "trung binh":
        full = string.ascii_lowercase + string.ascii_uppercase + string.digits
    else:
        # Tao ra 1 day string la tat ca bang chu cai viet thuong,
        # chu cai viet hoa
        full = string.ascii_lowercase + string.ascii_uppercase

    newpass = ''.join(random.choice(full) for i in range(length))
    return newpass


# Xuat ra de kiem tra
print(passWord(passLevel(), passLength()))
