'''
@author : Nguyen Phuoc Toan
@since : 21/02/2019
@version : 1.0

@exercise 14 : List Remove Duplicates
Write a program (function!) that takes a list and returns a new list
that contains all the elements of the first list minus all the duplicates.

Extras:

Write two different functions to do this -
one using a loop and constructing a list, and another using sets.
Go back and do Exercise 5 using sets, and write the solution
for that in a different function.
'''


# Nhap vao 1 list la kieu int, cac phan tu nen trung nhau
def createList():
    list1 = [int(x) for x in input("Nhap vao danh sach cac so : ").split()]
    return list1


# Cach 1: Xoa phan tu bi trung lap
# Dung set
def duplicate1(list1):
    list1 = set(list1)
    return print(str(list1))


# Cach 2: Xoa phan tu bi trung lap
# Dung vong lap for de kiem tra
def duplicate2(list1):
    for i in list1:
        if list1.count(i) != 1:
            list1.remove(i)
    return print(str(list1))


# Xuat ra ket qua de kiem tra
duplicate1(createList())
duplicate2(createList())
