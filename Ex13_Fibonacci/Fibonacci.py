'''
@author : Nguyen Phuoc Toan
@since : 21/02/2019
@version : 1.0

@exercise 13 : Fibonacci
Write a program that asks the user how many Fibonnaci numbers
to generate and then generates them. Take this opportunity to think about
how you can use functions. Make sure to ask the user to enter the number
of numbers in the sequence to generate.
(Hint: The Fibonnaci seqence is a sequence of numbers where
the next number in the sequence is the sum of the previous two numbers
in the sequence.
The sequence looks like this: 1, 1, 2, 3, 5, 8, 13, …)
'''


# Nhap vao 1 so
def createNum():
    num = int(input("Ban muon bao nhieu so trong fibonnaci xuat hien? "))
    return num


# Xuat so luong so fibonacci dua vao so num
def fibonnaci(num):
    list1 = [0, 1]
    count = 2
    num1 = 0
    num2 = 1
    sum_fib = 0
    # Kiem tra so num
    while num <= 0:
        num = int(input("So nhap vao phai lon hon 0 "))
    if num == 1:
        list1.remove(1)
    elif num >= 3:
        # Tinh so fibonacci
        while count < num:
            sum_fib = num2 + num1
            num1 = num2
            num2 = sum_fib
            list1.append(sum_fib)
            count += 1
    return print(str(list1))


# Xuat ra ket qua de kiem tra
fibonnaci(createNum())
