'''
@author : Nguyen Phuoc Toan
@since : 22/02/2019
@version : 1.0

@exercise 17 : Decode A Web Page
Use the BeautifulSoup and requests Python packages to print out
a list of all the article titles on the New York Times homepage.
'''
import requests
from bs4 import BeautifulSoup

# Truy xuat trang web
result = requests.get("https://www.nytimes.com/")

# Xuat ra ket qua la cac dong text
c = result.text
soup = BeautifulSoup(c, "html.parser")

# Truy xuat nhung tieu de la the h2
samples = soup.find_all('h2')
for sub_heading in samples:
    print(sub_heading.text)
