'''
@author : Nguyen Phuoc Toan
@since : 11/03/2019
@version : 1.0

@exercise 23 : File Overlap
Given two .txt files that have lists of numbers in them,
find the numbers that are overlapping. One .txt file has a
list of all prime numbers under 1000, and the other .txt file
has a list of happy numbers up to 1000.

(If you forgot, prime numbers are numbers that can’t be
divided by any other number.
And yes, happy numbers are a real thing in mathematics -
you can look it up on Wikipedia. The explanation is easier with an example,
which I will describe below.)
'''

# Doc file primenumbers.txt
list_primenum = []
with open('/hoctap/Python_Basic_Exercise/python_basicex/\
Ex23_FileOverlap/primenumbers.txt') as f:
    line = f.readline()
    while line:
        # Bo ki tu \n
        line = line.strip()
        # Dua noi dung doc duoc vao 1 list
        list_primenum.append(line)
        line = f.readline()

# Doc file happynumbers.txt
list_happynum = []
with open('/hoctap/Python_Basic_Exercise/python_basicex/\
Ex23_FileOverlap/happynumbers.txt') as f:
    line = f.readline()
    while line:
        # Bo ki tu \n
        line = line.strip()
        # Dua noi dung doc duoc vao 1 list
        list_happynum.append(line)
        line = f.readline()

# Tao 1 list luu cac so trung nhau
overlap_num = []

# Tao vong lap chay tren 2 list vua doc duoc
for i in list_primenum:
    for j in list_happynum:
        # Neu bang nhau thi dua vao list overlap_num
        if i == j:
            overlap_num.append(i)

# In ra de kiem tra
print(overlap_num)
