'''
@author : Nguyen Phuoc Toan
@since : 11/03/2019
@version : 1.0

@exercise 33 : Birthday Dictionairies
This exercise is Part 1 of 4 of the birthday data exercise series.
The other exercises are: Part 2, Part 3, and Part 4.

For this exercise, we will keep track of when our friend’s birthdays are,
and be able to find that information based on their name.
Create a dictionary (in your file) of names and birthdays.
When you run your program it should ask the user to enter a name,
and return the birthday of that person back to them.
The interaction should look something like this:

>>> Welcome to the birthday dictionary. We know the birthdays of:
Albert Einstein
Benjamin Franklin
Ada Lovelace
>>> Who's birthday do you want to look up?
Benjamin Franklin
>>> Benjamin Franklin's birthday is 01/17/1706.
Happy coding!
'''

dict1 = {"Toan": "28/01/1997", "Hung": "20/02/1997", "Dang": "02/09/1997"}

print("Chung toi co ngay sinh cua nhung nguoi sau: ")
for i in dict1:
    print(i)
name_find = input("Nhap ten nguoi ban muon tim ")
if name_find in dict1:
    print(name_find + " co ngay sinh la " + dict1[name_find])
else:
    print(name_find + " khong co trong danh sach")
