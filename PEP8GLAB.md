# Python Basic Exercise

Use pep8 to check code Python 3. on Gitlab

### Prerequisites

```
* Python 3.4 or later (http://www.python.org/)
```

### Installing


```
Python 3.4 or later (http://www.python.org/)
```

## Sources

- mittelab_party_italy:(https://git.mittelab.org/infra/tryton/mittelab_party_italy).

## Usage
```python

Git clone : (https://git.mittelab.org/infra/tryton/mittelab_party_italy).

Copy : All files and folders (Not folder doc)

Open file : .gitlab-ci.yml
	Delete all from Line 23
```

## Authors

* **Nguyen Phuoc Toan** - *Initial work* - [Gitlab](https://gitlab.com/a1kunkun).

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
