'''
@author : Nguyen Phuoc Toan
@since : 21/02/2019
@version : 1.0

@exercise 12 : List Ends
Write a program that takes a list of numbers
(for example, a = [5, 10, 15, 20, 25]) and makes a new list of only the first
and last elements of the given list.
For practice, write this code inside a function.
'''


# Nhap vao 1 list la kieu int
def createList():
    list1 = [int(x) for x in input("Nhap vao danh sach cac so : ").split()]
    return list1


# Dua phan tu dau va cuoi vao list moi
def firstLast(list1):
    new_list = []
    new_list.append(list1[0])
    new_list.append(list1[-1])
    return print(new_list)


# Xuat ra ket qua de kiem tra
firstLast(createList())
