'''
@author : Nguyen Phuoc Toan
@since : 19/02/2019
@version : 1.0

@exercise 2 : Odd or Even
Ask the user for a number.
Depending on whether the number is even or odd,
print out an appropriate message to the user.
Hint: how does an even / odd number react differently when divided by 2?

Extras:

- If the number is a multiple of 4, print out a different message.
- Ask the user for two numbers: one number to check (call it num)
and one number to divide by (check).
If check divides evenly into num, tell that to the user.
If not, print a different appropriate message.
'''
# Nhap so
num = int(input("Nhap vao 1 con so : "))

# Phan biet chan hay le
if num % 2 == 0:
    print("So ban vua nhap " + str(num) + " la so chan")
else:
    print("So ban vua nhap " + str(num) + " la so le")

# BT phan extras
# Kiem tra xem co phai boi cua 4 hay khong
if num % 4 == 0:
    print("So ban vua nhap la boi so cua 4")

# Nhap them so thu 2
check = int(input("Nhap them 1 con so : "))

if check % num == 0:
    print("So ban vua nhap chia het cho so truoc")
else:
    print("So ban vua nhap khong chia het cho so truoc")
