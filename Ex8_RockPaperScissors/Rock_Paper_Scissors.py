'''
@author : Nguyen Phuoc Toan
@since : 20/02/2019
@version : 1.0

@exercise 8 : Rock Paper Scissors
Make a two-player Rock-Paper-Scissors game.
(Hint: Ask for player plays (using input), compare them,
print out a message of congratulations to the winner,
and ask if the players want to start a new game)

Remember the rules:

- Rock beats scissors
- Scissors beats paper
- Paper beats rock
'''
# 2 Player nhap vao keo, bua, bao
user1 = input("Player1 vui long chon : keo, bua, bao : ")
user2 = input("Player2 vui long chon : keo, bua, bao : ")

# Tao list
a = ["keo", "bua", "bao"]
b = ["keo", "bua", "bao"]

# Kiem tra xem Player co nhap dung nhu yeu cau khong
for i in a and b:
    if a in a and b:
        pass

# So sanh input player nhap vao
if user1 == user2:
    print("Hai ben hue nhau, hay thu lai")
elif user1 == "keo":
    if user2 == "bua":
        print("Player2 thang, bua thang keo")
    elif user2 == "bao":
        print("Player2 thua, bao thua keo")
elif user1 == "bua":
    if user2 == "keo":
        print("Player2 thua, keo thua bua")
    elif user2 == "bao":
        print("Player2 thang, bao thang keo")
elif user1 == "bao":
    if user2 == "keo":
        print("Player2 thang, keo thang bao")
    elif user2 == "bua":
        print("Player2 thua, bua thua bao")
else:
    print("Vui long chi nhap : keo, bua, bao")
