'''
@author : Nguyen Phuoc Toan
@since : 11/03/2019
@version : 1.0

@exercise 24 : Draw A Game Board
This exercise is Part 1 of 4 of the Tic Tac Toe exercise series.
The other exercises are: Part 2, Part 3, and Part 4.

Time for some fake graphics!
Let’s say we want to draw game boards that look like this:

 --- --- ---
|   |   |   |
 --- --- ---
|   |   |   |
 --- --- ---
|   |   |   |
 --- --- ---
This one is 3x3 (like in tic tac toe).
Obviously, they come in many other sizes
(8x8 for chess, 19x19 for Go, and many more).

Ask the user what size game board they want to draw,
and draw it for them to the screen using Python’s print statement.

Remember that in Python 3,
printing to the screen is accomplished by

  print("Thing to show on screen")
Hint: this requires some use of functions,
as were discussed previously on this blog and elsewhere on the Internet,
like this TutorialsPoint link.
'''


# function in ra dong ---
def width_size():
    return print(' ---' * board_size)


# function in ra dong |
def height_size():
    return print('|   ' * (board_size + 1))


if __name__ == "__main__":
    board_size = int(input("Nhap vao kich thuoc ban muon:  "))
    # Tao ra vong lap theo kich thuoc da nhap
    for index in range(board_size):
        width_size()
        height_size()
    print(width_size())
