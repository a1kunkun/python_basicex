'''
@author : Nguyen Phuoc Toan
@since : 19/02/2019
@version : 1.0

@exercise 4 : Divisors
Create a program that asks the user for a number
and then prints out a list of all the divisors of that number.
(If you don’t know what a divisor is,
it is a number that divides evenly into another number.
For example, 13 is a divisor of 26 because 26 / 13 has no remainder.)
'''
# Tao list rong
b = []

# Nhap vao mot so
num = int(input("Nhap vao mot so : "))

# Tao list nho hoac bang so num
a = range(1, num+1)

# Xuat ra list a de kiem tra
print(a)

# Tim uoc cua so vua nhap
for i in range(len(a)):
    if num % a[i] == 0:
        # Luu ket qua vao list b
        b.append(a[i])

# Xuat ra list b de kiem tra
print(b)
