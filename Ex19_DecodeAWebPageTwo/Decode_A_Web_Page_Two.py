'''
@author : Nguyen Phuoc Toan
@since : 25/02/2019
@version : 1.0

@exercise 19 : Decode A Web Page Two
Using the requests and BeautifulSoup Python libraries,
print to the screen the full text of the article on this website:
http://www.vanityfair.com/society/2014/06/monica-lewinsky-humiliation-culture.

The article is long, so it is split up between 4 pages.
Your task is to print out the text to the screen so that
you can read the full article without having to click any buttons.

This will just print the full text of the article to the screen.
It will not make it easy to read, so next exercise we will learn
how to write this text to a .txt file.
'''

import requests
from bs4 import BeautifulSoup

# Truy xuat trang web
result = requests.get("http://www.vanityfair.com/society/2014/06"
                      "/monica-lewinsky-humiliation-culture")

# Xuat ra ket qua la cac dong text
c = result.text
soup = BeautifulSoup(c, "html.parser")

# Truy xuat noi dung bai viet
samples = soup.find_all('p')
for sub_text in samples:
    print(sub_text.text)
