'''
@author : Nguyen Phuoc Toan
@since : 11/03/2019
@version : 1.0

@exercise 31 : GuessLetters
This exercise is Part 2 of 3 of the Hangman exercise series.
The other exercises are: Part 1 and Part 3.

Let’s continue building Hangman.
In the game of Hangman, a clue word is given by the program
that the player has to guess, letter by letter.
The player guesses one letter at a time until the entire word has been guessed.
(In the actual game, the player can only guess
6 letters incorrectly before losing).

Let’s say the word the player has to guess is “EVAPORATE”.
For this exercise, write the logic that asks a player
to guess a letter and displays letters in the clue word
that were guessed correctly.For now, let the player guess an infinite number
of times until they get the entire word. As a bonus, keep track of the letters
the player guessed and display a different message if the player
tries to guess that letter again. Remember to stop the game
when all the lettershave been guessed correctly!
Don’t worry about choosing a word randomly or
keeping track of the number of guesses the player has remaining -
we will deal with those in a future exercise.

An example interaction can look like this:

>>> Welcome to Hangman!
_ _ _ _ _ _ _ _ _
>>> Guess your letter: S
Incorrect!
>>> Guess your letter: E
E _ _ _ _ _ _ _ E
...
And so on, until the player gets the word.
'''

word = "TOAN"

# Tao ra chuoi ki tu "-" kich thuoc bang voi kich thuoc cua word
hided = "-" * len(word)

# Chuyen word va hided thanh list
word = list(word)
hided = list(hided)
running = True
lstGuessed = []
letter = input("guess letter: ")
while running:
    # Kiem tra ki tu nhap vao da duoc nhap tu truoc chua
    if letter in lstGuessed:
        letter = ''
        print("Already guessed!!")
    # Kiem tra letter co trong word thi them letter vao hided
    elif letter in word:
        index = word.index(letter)
        hided[index] = letter
        word[index] = '-'
    else:
        print(' '.join(hided))
        if letter is not '':
            lstGuessed.append(letter)
        letter = input("guess letter: ")

    if '-' not in hided:
        print("You won!!")
        break
